# Scientific Figures in Inkscape
[![CC BY 4.0][cc-by-shield]][cc-by]

*This workshop was created by [Dominic Phillips](https://web.inf.ed.ac.uk/cdt/biomedical-ai/people/doctoral-researchers/2021-cohort), a Biomedical AI CDT Student at The University of Edinburgh. - October 2022*

By the end of this workshop, you will be able to start using the [Inkscape software](https://inkscape.org/) to create professional and informative research graphics that combine Latex, Python plots, and 2D and 3D drawings and annotations.

**Q) I already use PowerPoint to edit figures, why do I need to learn Inkscape?**

A) Inkscape is more powerful than PowerPoint for editing and manipulating graphics. It is also much easier to incorporate LaTeX code directly, rather than through awkward screenshotting and cut and paste.

**Q) Ok, but why use Inkscape and not Photoshop?**

A) It’s free! Also, LaTex again.

Besides being free and open-source, Inkscape also has ample community tutorial resources. Links to these resources can be found in the "Going Further" section at the end of this document. 

## Installing Inkscape (TODO before workshop)

*This tutorial assumes that you are using the latest version of Inkscape (Version 1.2 as of September 2022).*

As with most technical software, Inkscape is easier to install and customise when working in a Linux environment but it is also available for [Mac](https://inkscape-manuals.readthedocs.io/en/latest/installing-on-mac.html) and [Windows](https://inkscape-manuals.readthedocs.io/en/latest/installing-on-windows.html). 

To install Inkscape on Linux run:

`sudo add-apt-respository ppa:inkscape.dev/stable`

`sudo apt update`

`sudo apt-get install inkscape`

You can check that the installation was successful with

`inkscape --version`.


>💡 *Tip*: You can troubleshoot installation problems with the [official documentation](https://inkscape-manuals.readthedocs.io/en/latest/).


# 1. Inkscape Basics

## Working with text, shapes and images (15 min)

**Insert text** with <img src="assets/text.png"  width="25" height="25">, draw **squares** and **circles** with <img src="assets/square.png"  width="25" height="25"> and <img src="assets/circle.png"  width="25" height="25">. You can **scribble** and **free-draw** shapes with <img src="assets/scribble.png"  width="25" height="25"> and you can **curve and warp edges** with <img src="assets/node_path.png"  width="25" height="25">. 

**Change the colour** of a shape or its border with `right-click -> Fill and Stroke`.

**Round the corners** of a rectangle by selecting it with <img src="assets/node_path.png"  width="25" height="25"> and drag the white circle in the top-right-hand corner vertically downwards.

**Turn a line into an arrow** with `right-click -> Fill and Stroke -> Stroke Style -> Markers`.

**Import an image**, pdf or SVG file with `File -> Import...`.

**Group objects** by selecting them with <img src="assets/select.png"  width="25" height="25"> and `right-click -> Group` or `Object -> Group`.

**Resize the canvas** by selecting relevant objects with <img src="assets/select.png"  width="25" height="25">, then go `File -> Document Properties` and click the 'resize to content' button (<img src="assets/resize.png"  width="25" height="25">). 

## Cropping (10 min)

In Inkscape it is possible to crop arbitrary shapes out of images. However, the sacrifice for this generality is some added difficulty. In fact, there are [three entirely different ways to crop an image](https://www.selfmadedesigner.com/cropping-inkscape/). Here’s the simplest way:

<img style="float: right; padding-left: 15px" src="assets/1.png" alt="drawing" width="330" class="padding"/>

1) Prepare your image (here: happy scientist) and mask (here: yellow circle). Note that the color and style of the mask are unimportant, only its shape. 

<br/><br/>
<br/><br/>

<img style="float: right; padding-left: 15px" src="assets/2.png" alt="drawing" width="330" class="padding"/>

2) Place the mask in the desired place *on top of* the image. Select both the image and the mask.

<br/><br/>
<br/><br/>
<br/><br/>

<img style="float: right; padding-left: 15px" src="assets/3.png" alt="drawing" width="330" class="padding"/>

3) No go to **Object > Clip > Set Clip** and voilà!


<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>

## Vectorizing Images (15 min)

Often we find ourselves looking to repurpose png/JPEG images from online to put in scientific figures. But what if they are too blurry or unprofessional looking for the final paper? Enter your new best friend: *image vectorization*! 

![1.png](assets/neuron_vectorised.png)

To **vectorize an image** select it with <img src="assets/select.png"  width="25" height="25"> and go to `Path -> Trace Bitmap...`. Select `Multiple scans` and change the top drop-down box to `Colors`. You can experiment with the number of scans and additional settings. For crisper, less-cluttered vectorised images its a good idea to enable `Stack` and `Remove background`. Other options can be kept at their default (`Scans=8`, `Speckles=2`, `Smooth corners=1.00`, and `Optimize=0.20`). Create the verorization by clicking `Update` and then `Apply`. 


>💡 *Tip*: The `Scans` variable sets the number of layers in the final vectorized object. You might want to experiment making this as small as possible whilst skill capturing the full range of colors in your image. If you final vectorised image still looks a little messy, then experiment with manually deleting or modifying any unnecessary layers.

# 3. Working with LaTeX

Getting LaTeX to work in Inkscape requires some additional installations. The instructions below assume that you are using Linux. Click the links for detailed instructions for [Mac](https://textext.github.io/textext/install/macos.html) and [Windows](https://textext.github.io/textext/install/windows.html). 

## Installing TexText (TODO before workshop)
1. Make sure that an operational LaTeX distribution is installed on your system. Verify this by invoking at least one of `pdflatex --version` , `xelated --version` ,`lualatex --version`.
    
    If not already installed, then `sudo apt-get install texlive-latex-base` should do the trick.
    
2. Install GTKSourceView (enables syntax highlighting) with
    
     `sudo apt install gir1.2-gtksource-3.0`.
    
3. Download and install [TexText](https://github.com/textext/textext/releases/download/1.8.2/TexText-Linux-1.8.2.zip) by extracting the package and running  `python3 [setup.py](http://setup.py)` from the terminal in the package directory. If successful, you should see
    
    `[TexText][SUCCESS ]: --> TexText has been SUCCESSFULLY installed on your system <—`
    
    as the last output line onto the terminal. 
    
4. Verify the installation by going to **Extensions > Text > TexText** in Inkscape.

## Using TexText with custom preamble (15 min) 

In our LaTeX projects, we often have preamble code with custom packages, definitions and styling. 

The default preamble, shown below, is ok for basic maths but is otherwise a little stark:

    \documentclass{article} 
    \usepackage{amsmath,amsthm,amssymb,amsfonts}
    \usepackage{color}

To integrate your preferred preamble into Inkscape simply create/locate your .tex preamble file and import it into TexTex by clicking the “Preamble File” file in the top left of the TexText window.


> 💡 *Tip*: If you’re an avid Overleaf user, then you might find that your offline Latex installation doesn’t have all of your favourite packages preinstalled. If TexText gives you a package error, you can always install the missing package by executing `tlmgr install <package>`. Click [here](https://tug.org/texlive/tlmgr.html) for more information on the tlmgr package.

## Putting it All Together (30 min)

Now over to you! Use all your Inkscape skills to complete the missing parts of `figure_in_progress.pdf` to make it look as close as possible to `figure_complete.pdf`. The files you will need to help you do this are `free_energy_surface.pdf` and `diffusion_smooth.pdf`.

## Going Further

If you’re feeling inspired then a good place to start to learn more about the power of Inkscape is:

[https://inkscape.org/learn/](https://inkscape.org/learn/) and [https://inkscape.org/gallery/=tutorial/?license=CC-BY-SA 4.0](https://inkscape.org/gallery/=tutorial/?license=CC-BY-SA%204.0).

# License
The work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg

