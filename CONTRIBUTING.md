The tutorial was developed by **Dominic Phillips**.

GitHub handle: **dominicp6**

Twitter handle: **domfephillips**

Academic affiliations:

Mey Research Group in Computational Biophysics https://mey-research.org/team/

Biomedical AI CDT Student https://web.inf.ed.ac.uk/cdt/biomedical-ai/people/doctoral-researchers/2021-cohort

